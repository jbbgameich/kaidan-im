---
layout: post
title: "Kaidan is now available on the OpenStore"
date: 2019-04-17 20:00:00 +02:00
author: lnj
---

Upon the incredible interest from the [Fediverse][kaidan-mastodon], we now got the confirmation that Kaidan actually "works" on real Ubuntu Touch devices. We decided to publish Kaidan on Ubuntu Touch's official store now. You can find it [here][openstore].

![Kaidan on the OpenStore](/images/2019/04/17/kaidan-openstore.png)

We're aware of some bugs: e.g. the keyboard is still overlapping input fields and the icons are also not perfect. We hope to get those things fixed, but don't expect too much. On Ubuntu Touch and also in general Kaidan is better than nothing, but also far from being a mature, full-featured client.

Feel free to report all bugs you notice on [GitLab][gitlab]!

[kaidan-mastodon]: https://fosstodon.org/@kaidan
[openstore]: https://open-store.io/app/im.kaidan.kaidan
[gitlab]: https://invent.kde.org/KDE/kaidan

