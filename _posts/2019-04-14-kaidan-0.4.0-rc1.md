---
layout: post
title: "Easter hunt: Find the new hidden bugs in Kaidan 0.4.0~rc1!"
date: 2019-04-14 23:00:00 +02:00
author: lnj
---

After 18 months of development and nearly 400 commits (that are more new commits than all commits from all releases before) we had the absurd idea to make a new release. And here it is .. or well, at least a release candidate. We know that there are still some bugs left, so we'd be happy if you could report everything you notice, we'll try to fix everything and then we'll do a proper release.

Most important changes in the new release are:
* Experimental Android, Ubuntu Touch, iOS, macOS and Windows support
* Media sharing: you can upload files and images
* Last Message Correction
* Spoiler messages
* Links in the chat are clickable now
* Presence of contacts is shown
* Mobile traffic optimizations
* Many new translations
* Small usability improvements
* Many UI design improvements

A full change log will follow with the actual release.

Bug reports go to [GitLab](https://invent.kde.org/KDE/kaidan/issues) as always and translations are managed on [Weblate](https://hosted.weblate.org/projects/kaidan/translations/).

The Kaidan team wishes happy Easter!

**Download**:
* Source code: [kaidan-v0.4.0-rc1.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.4.0-rc1/kaidan-v0.4.0-rc1.tar.gz)
* Linux (AppImage): [kaidan_0.4.0~rc1_x86_64.AppImage](https://archive.kaidan.im/kaidan/0.4.0-rc1/kaidan_0.4.0~rc1_x86_64.AppImage)
* Windows (x64): [kaidan_0.4.0~rc1_x86_64.exe](https://archive.kaidan.im/kaidan/0.4.0-rc1/kaidan_0.4.0~rc1_x86_64.exe)
* Ubuntu Touch (untested): [im.kaidan.kaidan_0.4.0~rc1_armhf.click](https://archive.kaidan.im/kaidan/0.4.0-rc1/im.kaidan.kaidan_0.4.0~rc1_armhf.click)
* Flatpak builds are only available as nightly: [Installing the Kaidan Flatpak](https://invent.kde.org/KDE/kaidan/wikis/install/flatpak)

PS: We're searching for someone with an Ubuntu Touch device who can regularly test builds: [contact us](https://i.kaidan.im)!
