---
layout: page
title: Development
permalink: /development/
sitemap: true
---

Our source code is managed using Git. You can browse it on [our GitLab instance](https://git.kaidan.im/kaidan/kaidan) or clone it locally using:

{% highlight bash %}
git clone https://git.kaidan.im/kaidan/kaidan
{% endhighlight %}

### Contributing

Issues and code review is done on GitLab. You need to either register on our instance or log in with a GitLab.com or GitHub account to start contributing.

### Contact

You can get in touch with us in our [XMPP channel](https://i.kaidan.im/#kaidan@muc.kaidan.im?join).

