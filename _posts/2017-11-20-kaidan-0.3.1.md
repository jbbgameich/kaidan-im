---
layout: post
title: "Kaidan 0.3.1 released"
date: 2017-11-20 18:18:00 +01:00
author: lnj
---

## Changelog

Fixes:
 * Fixed database creation errors (#135, #132) (LNJ)
 * ChatPage: Fixed recipient's instead of author's avatar displayed (#131, #137) (LNJ)

Misc:
 * Added Travis-CI integration (#133, #134, #136) (JBB)

Internationalization:
 * Added Malay translations (#129) (MuhdNurHidayat)

**Download**: [kaidan-v0.3.1.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.3.1/kaidan-v0.3.1.tar.gz), [kaidan-x86_64.AppImage](https://archive.kaidan.im/kaidan/0.3.1/kaidan-x86_64.AppImage)
